# PickiT


#### 项目介绍
- 项目名称：该库可通过文件的Uri获取到文件的path值
- 所属系列：openharmony的第三方组件适配移植
- 功能：该库可通过文件的Uri获取到文件的path功能
- 项目移植状态：暂时只支持部分图片音视频文件
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 beta4
- 基线版本： Release 0.1.14

#### 效果演示
![效果演示](printscreen/PickiT.gif)



#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
 }
 ```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:PickiT:0.0.1-SNAPSHOT')
    ......  
 }
 ```

#### 使用说明

首先，实现PickiT回调，如下所示:

```java
public class MainAbilitySlice extends AbilitySlice implements PickiTCallbacks {
```

`Alt+Enter` 为了实现这些方法，我们将在自述文件的后面部分讨论这些方法.

在 `onCreate()`方法中实现pickiT，如下所示:

```java
public class MainAbilitySlice extends AbilitySlice implements PickiTCallbacks {
    //Declare PickiT
    PickiT pickiT;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        //Initialize PickiT
        //context, listener, AbilitySlice
        pickiT = new PickiT(this, this, this);

    }
}
```

现在您可以像平常一样选择一个文件（如果您不知道如何做，请看演示）.

然后在`onAbilityResult`中，可以将路径传递给PickiT，如下所示:

```java
@Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (requestCode == SELECT_VIDEO_REQUEST) {
            if (resultCode == RESULT_OK && resultData.getUri() != null) {
                pickiT.getPath(resultData.getUri());
                originalText.setText(String.valueOf(resultData.getUri()));
            }
        }
    }
```

在处理完文件后通过调用:

```java
pickiT.deleteTemporaryFile(Context);
```

这可以在 `onBackPressed` 和 `onStop` 中完成，如下所示:

```java
@Override
protected void onBackPressed() {
    pickiT.deleteTemporaryFile(this);
    super.onBackPressed();
}

 @Override
 protected void onStop() {
    super.onStop();
    if (!isChangingConfigurations()) {
        pickiT.deleteTemporaryFile(this);
    }
}
```

如果不调用 `pickiT.deleteTemporaryFile(Context);`, 该文件将保留在上述文件夹中，并且每次选择新文件时都会被覆盖.

回调方法

```java
//When selecting a file from Drive, for example, the Uri will be returned before the file is available(if it has not yet been cached/downloaded).
//We are unable to see the progress
//Apps like Dropbox will display a dialog inside the picker
//This will only be called when selecting a drive file
@Override
public void PickiTonUriReturned() {
    //Use to let user know that we are waiting for the application to return the file
    //See the demo project to see how I used this.
}

//Called when the file creations starts (similar to onPreExecute)
//This will only be called if the selected file is not local or if the file is from an unknown file provider
@Override
public void PickiTonStartListener() {
    //Can be used to display a ProgressDialog
}

//Returns the progress of the file being created (in percentage)
//This will only be called if the selected file is not local or if the file is from an unknown file provider
@Override
public void PickiTonProgressUpdate(int progress) {
    //Can be used to update the progress of your dialog
}

//If the selected file was a local file then this will be called directly, returning the path as a String.
//String path - returned path
//boolean wasDriveFile - check if it was a drive file
//boolean wasUnknownProvider - check if it was from an unknown file provider
//boolean wasSuccessful - check if it was successful
//String reason - the get the reason why wasSuccessful returned false
@Override
public void PickiTonCompleteListener(String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String reason) {
    //Dismiss dialog and return the path
}
```

 如果您在实现库时遇到任何问题，请查看演示项目. 

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

火绒安全病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 0.0.1-SNAPSHOT

#### 版权和许可信息
```
Author: [Hagen Brooks](https://github.com/HBiSoft).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```