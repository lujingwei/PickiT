package com.hbisoft.pickitexample;

import com.hbisoft.pickitexample.slice.MainAbilitySlice;
import com.hbisoft.pickitexample.utils.PermissionBridge;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

import static com.hbisoft.pickitexample.utils.PermissionBridge.EVENT_PERMISSION_DENIED;
import static com.hbisoft.pickitexample.utils.PermissionBridge.EVENT_PERMISSION_GRANTED;
import static ohos.bundle.IBundleManager.PERMISSION_GRANTED;

public class MainAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#2980b9"));
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != MainAbilitySlice.PERMISSION_REQ_CODE) {
            return;
        }
        for (int grantResult : grantResults) {
            if (grantResult != PERMISSION_GRANTED) {
                PermissionBridge.getHandler().sendEvent(EVENT_PERMISSION_DENIED);
                return;
            }
        }
        PermissionBridge.getHandler().sendEvent(EVENT_PERMISSION_GRANTED);
    }

}
