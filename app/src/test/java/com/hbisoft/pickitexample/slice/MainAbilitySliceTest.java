package com.hbisoft.pickitexample.slice;

import junit.framework.TestCase;

public class MainAbilitySliceTest extends TestCase {

    public void testOnStart() {
    }

    public void testOnAbilityResult() {
    }

    public void testOnActive() {
    }

    public void testOnForeground() {
    }

    public void testPickiTonUriReturned() {
    }

    public void testPickiTonStartListener() {
    }

    public void testPickiTonProgressUpdate() {
    }

    public void testPickiTonCompleteListener() {
    }

    public void testOnBackPressed() {
    }

    public void testOnStop() {
    }

    public void testOnPermissionGranted() {
    }

    public void testOnPermissionDenied() {
    }
}